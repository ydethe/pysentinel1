import os
from struct import unpack, calcsize
from dataclasses import dataclass, InitVar
from fractions import Fraction
import io
from typing import BinaryIO
from datetime import datetime, timedelta

from tqdm import tqdm
from bitstring import BitArray
import numpy as np
from numpy import pi, exp
from scipy.signal import correlate
from matplotlib import pyplot as plt

from .ISPenum import (
    ECCNumber,
    RXChannel,
    WordIndex,
    RadarErrorFlag,
    BAQMode,
    SARSwath,
    DECIM_RATIO_DICT,
    FILTER_BW_DICT,
    FILTER_LENGTH_DICT,
    FILTER_OUTPUT_OFFSETS,
    D_VALUES,
    SASMode,
    Polarisation,
    SASTestMode,
    CalibType,
    SignalType,
)
from .decodeSamples import decodeFormatDSamples


# https://sentinel.esa.int/documents/247904/4629273/Sentinel-1-Level-1-Detailed-Algorithm-Definition-v2-3.pdf/3ca88b95-770a-37c3-1b45-0031869d344a
F_REF = 37.53472224


def int24_to_int(raw: bytes) -> int:
    (res,) = unpack(">I", b"\x00" + raw)
    return res


@dataclass(init=False, repr=True)
class PVTData:
    id: int
    time: float
    px: float
    py: float
    pz: float
    vx: float
    vy: float
    vz: float


@dataclass(init=False, repr=True)
class AttitudeData:
    id: int
    time: float
    q0: float
    qx: float
    qy: float
    qz: float
    wx: float
    wy: float
    wz: float


@dataclass(init=False, repr=True)
class SASDataImag:
    polar: Polarisation
    temp_comp_antenna_fe: bool
    temp_comp_antenna_ta: bool
    elev_beam_addr: int
    azim_beam_addr: int

    @classmethod
    def fromBuffer(cls, buffer: bytes, RXCHID: int):
        dat = cls()

        polar_code = (buffer[0] >> 4) & 0b111
        if polar_code == 0:
            assert RXCHID == 1
        elif polar_code == 1:
            assert RXCHID == 0
        elif polar_code == 5:
            assert RXCHID == 1
        elif polar_code == 6:
            assert RXCHID == 0
        dat.polar = Polarisation(polar_code)

        temp_comp_code = (buffer[0] >> 2) & 0b11
        if temp_comp_code == 0:
            dat.temp_comp_antenna_fe = False
            dat.temp_comp_antenna_ta = False
        elif temp_comp_code == 1:
            dat.temp_comp_antenna_fe = True
            dat.temp_comp_antenna_ta = False
        elif temp_comp_code == 2:
            dat.temp_comp_antenna_fe = False
            dat.temp_comp_antenna_ta = True
        elif temp_comp_code == 3:
            dat.temp_comp_antenna_fe = True
            dat.temp_comp_antenna_ta = True

        dat.elev_beam_addr = buffer[1] >> 4

        ABADR_code = (buffer[1] & 0b11) << 8 + buffer[2]
        dat.azim_beam_addr = ABADR_code

        return dat


@dataclass(init=False, repr=True)
class SASDataCalib:
    polar: Polarisation
    temp_comp_antenna_fe: bool
    temp_comp_antenna_ta: bool
    sas_test_mode: SASTestMode
    calib_type: CalibType
    calib_beam_addr: int

    @classmethod
    def fromBuffer(cls, buffer: bytes, RXCHID: int):
        dat = cls()

        polar_code = (buffer[0] >> 4) & 0b111
        if polar_code == 0:
            assert RXCHID == 1
        elif polar_code == 1:
            assert RXCHID == 0
        elif polar_code == 5:
            assert RXCHID == 1
        elif polar_code == 6:
            assert RXCHID == 0
        dat.polar = Polarisation(polar_code)

        temp_comp_code = (buffer[0] >> 2) & 0b11
        if temp_comp_code == 0:
            dat.temp_comp_antenna_fe = False
            dat.temp_comp_antenna_ta = False
        elif temp_comp_code == 1:
            dat.temp_comp_antenna_fe = True
            dat.temp_comp_antenna_ta = False
        elif temp_comp_code == 2:
            dat.temp_comp_antenna_fe = False
            dat.temp_comp_antenna_ta = True
        elif temp_comp_code == 3:
            dat.temp_comp_antenna_fe = True
            dat.temp_comp_antenna_ta = True

        SAStest = buffer[1] >> 7
        dat.sas_test_mode = SASTestMode(SAStest)

        cal_typ_code = (buffer[1] >> 4) & 0b111
        dat.calib_type = CalibType(cal_typ_code)

        CBADR_code = (buffer[1] & 0b11) << 8 + buffer[2]
        dat.calib_beam_addr = CBADR_code

        return dat


@dataclass(init=False, repr=True)
class SESData:
    cal_mod: int
    tx_pulse_num: int
    signal_type: SignalType
    swap: bool
    swath: int

    @classmethod
    def fromBuffer(cls, buffer: bytes):
        dat = cls()

        dat.cal_mod = buffer[0] >> 6
        dat.tx_pulse_num = buffer[0] & 0b11111

        SIGTYP_code = (buffer[1] >> 4) & 0b1111
        dat.signal_type = SignalType(SIGTYP_code)

        dat.swap = (buffer[1] & 0b1) == True

        dat.swath = buffer[2]

        return dat


@dataclass(init=False, repr=True)
class RadarConf:
    error_flag: RadarErrorFlag
    baq_mode: BAQMode
    data_format: str
    baq_length: int  # Number of complex radar samples per BAQ block
    sar_swath: SARSwath
    filter_bw: float
    decim_ratio: Fraction
    samp_freq: float
    filter_length: int  # Filter Length NF is given for a sampling frequency of 4 * fref
    rx_gain: float
    tx_pulse_ramp_rate: float
    tx_pulse_start_freq: float
    tx_pulse_length: float
    rank: int
    pulse_repetition_interval: float
    sampling_window_start_time: float
    dt_suppr: float
    sampling_window_length: float
    number_samp: int
    sas_mode: SASMode
    sas_data_imag: SASDataImag
    sas_data_calib: SASDataCalib
    ses_data: SESData
    number_quad: int

    @classmethod
    def fromBuffer(cls, buffer: bytes, RXCHID: int):
        rc = RadarConf()
        rc.error_flag = RadarErrorFlag(buffer[0] >> 7)
        baq_mode = buffer[0] & 0b11111
        rc.baq_mode = BAQMode(baq_mode)
        if baq_mode in [3, 4, 5]:
            rc.data_format = "C"
        elif baq_mode in [12, 13, 14]:
            rc.data_format = "D"
        else:
            rc.data_format = ""

        raw = buffer[1:9]
        (
            BAQBL_code,
            _,
            RGDEC_code,
            RXG_code,
            TXPRR_code,
            TXPSF_code,
        ) = unpack(">BBBBHH", raw)
        rc.baq_length = (BAQBL_code + 1) * 8
        assert rc.baq_length == 256
        rc.sar_swath = SARSwath(RGDEC_code)
        rc.filter_bw = FILTER_BW_DICT[RGDEC_code] * 1e6
        rc.decim_ratio = DECIM_RATIO_DICT[RGDEC_code]
        rc.samp_freq = 4 * F_REF * float(DECIM_RATIO_DICT[RGDEC_code]) * 1e6
        rc.filter_length = FILTER_LENGTH_DICT[RGDEC_code]

        rc.rx_gain = 10 ** ((-0.5 * RXG_code) / 10)

        P = TXPRR_code >> 15
        S = 1 - P
        TXPRR_code = TXPRR_code << 1
        TXPRR = (-1) ** S * TXPRR_code * F_REF ** 2 / 2 ** 21  # In MHz/µs
        rc.tx_pulse_ramp_rate = TXPRR * 1e12

        P = TXPSF_code >> 15
        S = 1 - P
        TXPSF_code = TXPSF_code << 1
        TXPSF = TXPRR / (4 * F_REF) + (-1) ** S * TXPSF_code * F_REF / 2 ** 14  # In MHz
        rc.tx_pulse_start_freq = TXPSF * 1e6

        TXPL_code = int24_to_int(buffer[9:12])
        TXPL = TXPL_code / F_REF  # In µs
        rc.tx_pulse_length = TXPL * 1e-6

        rc.rank = buffer[12] << 3

        PRI_code = int24_to_int(buffer[13:16])
        PRI = PRI_code / F_REF  # In µs
        rc.pulse_repetition_interval = PRI * 1e-6

        SWST_code = int24_to_int(buffer[16:19])
        SWST = SWST_code / F_REF  # In µs
        rc.sampling_window_start_time = SWST * 1e-6
        rc.dt_suppr = 320 / (8 * F_REF) * 1e-6

        SWL_code = int24_to_int(buffer[19:22])
        SWL = SWL_code / F_REF  # In µs
        rc.sampling_window_length = SWL * 1e-6

        L, M = rc.decim_ratio.as_integer_ratio()
        foo = FILTER_OUTPUT_OFFSETS[RGDEC_code]
        B = 2 * SWL_code - foo - 17
        C = B - M * int(B / M)
        D = D_VALUES[(RGDEC_code, C)]
        N3_rx = 2 * (L * int(B / M) + D + 1)
        rc.number_samp = N3_rx

        # Reading SAS SSB Data
        raw = buffer[22:25]
        SSBFLAG = raw[0] >> 7
        rc.sas_mode = SASMode(SSBFLAG)

        if SSBFLAG == 0:
            rc.sas_data_imag = SASDataImag.fromBuffer(raw, RXCHID=RXCHID)
            rc.sas_data_calib = None
        elif SSBFLAG == 1:
            rc.sas_data_imag = None
            rc.sas_data_calib = SASDataCalib.fromBuffer(raw, RXCHID=RXCHID)

        raw = buffer[25:28]
        rc.ses_data = SESData.fromBuffer(raw)

        # Number of quads.
        # A Quad is defined as a quadrupel of sample values, namely:
        # * 1 I-Part even sample value (IE)
        # * 1 I-part odd sample value  (IO)
        # * 1 Q-Part even sample value (QE)
        # * 1 Q-part odd sample value  (QO)
        # A complex radar sample is composed of one I-part and one Q-part sample value, respectively.
        # Consequently, the number of overall complex radar samples in the packet can be expressed as
        # the doubled value of the Number of Quads.
        # The number NSAMP of complex radar samples can be expressed as NSAMP = 2 * NQ
        raw = buffer[28:30]
        (NQ,) = unpack(">H", raw)
        rc.number_quad = NQ

        return rc


@dataclass(init=False, repr=True)
class Header:
    packet_version_number: int
    packet_type: int
    secondary_header_flag: int
    PID: int
    PCAT: int
    sequence_flags: int
    packet_sequence_count: int
    packet_data_length: int
    time: float
    ecc: ECCNumber
    rx_channel: RXChannel
    data_word_index: WordIndex
    data_word: bytes
    space_packet_count: int
    PRI_count: int
    radar_conf: RadarConf


@dataclass(init=False, repr=True)
class InstrumentSourcePacket:
    header: Header
    packet_total_length: int
    stream: InitVar[io.BytesIO]
    offset: int
    samples: np.array

    @classmethod
    def fromFile(
        cls, f: BinaryIO, offset: int, read_samples: bool = False
    ) -> "InstrumentSourcePacket":
        sze = os.fstat(f.fileno()).st_size
        if offset == sze:
            return None

        pow_2 = 2 ** np.arange(13, -1, -1)

        isp = cls()
        isp.stream = f
        isp.offset = offset

        # ========================================
        # Reading Headers
        # ========================================
        hdr = Header()

        f.seek(offset)
        raw = f.read(2)
        if raw != b"\x0c\x1c":
            raise AssertionError(raw, offset)

        hdr.packet_version_number = 0
        hdr.packet_type = 0
        hdr.secondary_header_flag = 1
        hdr.PID = 65
        hdr.PCAT = 12
        hdr.sequence_flags = 3

        raw = f.read(2)
        b = BitArray(raw)
        assert b[0] and b[1]

        hdr.packet_sequence_count = np.array(b[2:], dtype=np.uint8) @ pow_2

        raw = f.read(2)
        (hdr.packet_data_length,) = unpack(">H", raw)
        hdr.packet_data_length += 1

        isp.packet_total_length = 6 + hdr.packet_data_length
        assert isp.packet_total_length % 4 == 0

        raw = f.read(6)
        TCOAR, TFINE = unpack(">LH", raw)
        hdr.time = TCOAR + (TFINE + 0.5) / 2 ** 16

        # Skipping Sync Marker and Data Take ID
        raw = f.read(8)

        # Reading ECC Number
        raw = f.read(1)
        (ecc,) = unpack(">B", raw)
        hdr.ecc = ECCNumber(ecc)

        # Reading RX channel
        raw = f.read(1)
        b = BitArray(raw)
        RXCHID = np.array(b[4:], dtype=np.uint8) @ pow_2[-4:]
        hdr.rx_channel = RXChannel(RXCHID)

        # Skipping Instrument Configuration ID
        raw = f.read(4)

        # Reading Data Word Index
        raw = f.read(1)
        (dw_index,) = unpack(">B", raw)
        if dw_index < 42:
            hdr.data_word_index = WordIndex(dw_index)
        else:
            hdr.data_word_index = None

        # Reading Data Word
        hdr.data_word = f.read(2)

        # Reading Space Packet Count
        raw = f.read(8)
        hdr.space_packet_count, hdr.PRI_count = unpack(">LL", raw)

        raw = f.read(67 - 37 + 1)
        hdr.radar_conf = RadarConf.fromBuffer(raw, RXCHID=RXCHID)
        isp.header = hdr

        # ========================================
        # Reading User Data
        # ========================================
        if read_samples:
            if isp.header.radar_conf.data_format != "D":
                # Type A, B, and C (test modes) are not implemented
                return isp

            data_sze = isp.packet_total_length - 67
            raw = f.read(data_sze)
            if len(raw) != data_sze:
                raise AssertionError(
                    "Not enough data read: %i for %i asked" % (len(raw), data_sze)
                )
            NQ = hdr.radar_conf.number_quad
            baq_mode = hdr.radar_conf.baq_mode
            isp.samples = decodeFormatDSamples(NQ, baq_mode, raw)
        else:
            isp.samples = None

        return isp
