import os
import yaml

from .ISPenum import BAQMode, SARSwath, SignalType


__all__ = ["loadConfig"]


def loadConfig(path: str) -> dict:
    """Reads a Yaml config file

    Args:
      path
        The path to the config file

    Returns:
      A dictionary containing the configuration

    Examples:
      >>> cfg = loadConfig('tests/config.yml')

    """
    with open(path, "r") as stream:
        cfg = yaml.load(stream, Loader=yaml.FullLoader)
    cfg["baq_mode"] = BAQMode.__members__[cfg["baq_mode"]]
    cfg["sar_swath"] = SARSwath.__members__[cfg["sar_swath"]]
    cfg["signal_type"] = SignalType.__members__[cfg["signal_type"]]
    cfg["binary"] = os.path.expanduser(cfg["binary"])
    return cfg


if __name__ == "__main__":
    import doctest

    doctest.testmod()
