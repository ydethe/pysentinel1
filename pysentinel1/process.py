import os
from pathlib import Path
from datetime import datetime
from typing import Tuple

import numpy as np
import pandas as pd
from pandas.core.frame import DataFrame
from blocksim.dsp.DSPSignal import DSPSignal

from . import logger
from .InstrumentSourcePacket import InstrumentSourcePacket
from .readSamples import readBatchSamples
from .downloadOrbitFile import listOrbitFiles, downloadOrbit
from .readOrbit import readOrbit


__all__ = ["processDataTake"]


def processDataTake(cfg: dict, export_samples: bool = True):
    """This function analyses the raw sentinel file. It creates :

    * header.parquet, to store the metadata of the data take
    * orbit.parquet, to store the precise orbit during the data take

    The config file stores the criteria to filter the data takes in the file.

    Args:
      cfg
        The configuration, as loaded by :func:`pysentinel1.config.loadConfig`
      export_samples
        To deactivate the reading of the IQ samples

    """
    folder = Path(cfg["output"])
    folder.mkdir(parents=True, exist_ok=True)
    bn = os.path.basename(cfg["binary"])
    if bn.lower().startswith("s1a"):
        platformnumber = "A"
    elif bn.lower().startswith("s1b"):
        platformnumber = "B"
    else:
        raise AssertionError("Raw file does not start with s1a nor with s1b" % bn)

    logger.info("Skipping the first swath that might be incomplete")
    binary_strm = open(cfg["binary"], "rb")
    isp = InstrumentSourcePacket.fromFile(f=binary_strm, offset=0, read_samples=False)
    init_swath = isp.header.radar_conf.ses_data.swath

    # Skip the first swath that might be incomplete
    offset = isp.packet_total_length
    while isp.header.radar_conf.ses_data.swath == init_swath:
        isp = InstrumentSourcePacket.fromFile(
            f=binary_strm, read_samples=False, offset=offset
        )
        offset += isp.packet_total_length

    logger.info(
        "Filter following the config file to find the beginning of the interest subset of the file"
    )
    # Filter following the config file to find the beginning of the interest subset of the file
    while not (
        isp.header.radar_conf.baq_mode == cfg["baq_mode"]
        and isp.header.radar_conf.data_format == cfg["data_format"]
        and isp.header.radar_conf.sar_swath == cfg["sar_swath"]
        and isp.header.radar_conf.ses_data.signal_type == cfg["signal_type"]
        and isp.header.radar_conf.ses_data.swath == cfg["swath"]
    ):
        isp = InstrumentSourcePacket.fromFile(
            f=binary_strm, read_samples=False, offset=offset
        )
        offset += isp.packet_total_length

    prec_ebadr = isp.header.radar_conf.sas_data_imag.elev_beam_addr
    offset0 = offset

    logger.info("Loading the metadata of the interest subset of the file")
    # Filter following the config file to find the end of the interest subset of the file
    columns = [
        "offset",
        "data_word_index",
        "ecc",
        "packet_data_length",
        "packet_sequence_count",
        "packet_version_number",
        "PRI_count",
        "rx_channel",
        "space_packet_count",
        "UTC",
        "baq_length",
        "baq_mode",
        "data_format",
        "decim_ratio",
        "dt_suppr",
        "filter_bw",
        "filter_length",
        "number_samp",
        "pulse_repetition_interval",
        "rank",
        "rx_gain",
        "samp_freq",
        "sampling_window_length",
        "sampling_window_start_time",
        "sar_swath",
        "tx_pulse_length",
        "tx_pulse_ramp_rate",
        "tx_pulse_start_freq",
        "elev_beam_addr",
        "azim_beam_addr",
        "signal_type",
        "tx_pulse_num",
        "swap",
        "swath",
    ]
    rows = []

    t0_gps = datetime(year=1980, month=1, day=6).timestamp()
    while (
        isp.header.radar_conf.baq_mode == cfg["baq_mode"]
        and isp.header.radar_conf.data_format == cfg["data_format"]
        and isp.header.radar_conf.sar_swath == cfg["sar_swath"]
        and isp.header.radar_conf.ses_data.signal_type == cfg["signal_type"]
        and isp.header.radar_conf.ses_data.swath == cfg["swath"]
        and isp.header.radar_conf.sas_data_imag.elev_beam_addr - prec_ebadr >= 0
    ):
        if not isp.header.radar_conf.sas_data_imag is None and False:
            elev_beam_addr = isp.header.radar_conf.sas_data_imag.elev_beam_addr
            azim_beam_addr = isp.header.radar_conf.sas_data_imag.azim_beam_addr
        else:
            elev_beam_addr = -1
            azim_beam_addr = -1

        if isp.header.data_word_index is None:
            dwi = -1
        else:
            dwi = isp.header.data_word_index.value

        dat = (
            isp.offset,
            dwi,
            isp.header.ecc.value,
            isp.header.packet_data_length,
            isp.header.packet_sequence_count,
            isp.header.packet_version_number,
            isp.header.PRI_count,
            isp.header.rx_channel.value,
            isp.header.space_packet_count,
            isp.header.time + t0_gps,
            isp.header.radar_conf.baq_length,
            isp.header.radar_conf.baq_mode.value,
            isp.header.radar_conf.data_format,
            float(isp.header.radar_conf.decim_ratio),
            isp.header.radar_conf.dt_suppr,
            isp.header.radar_conf.filter_bw,
            isp.header.radar_conf.filter_length,
            isp.header.radar_conf.number_samp,
            isp.header.radar_conf.pulse_repetition_interval,
            isp.header.radar_conf.rank,
            isp.header.radar_conf.rx_gain,
            isp.header.radar_conf.samp_freq,
            isp.header.radar_conf.sampling_window_length,
            isp.header.radar_conf.sampling_window_start_time,
            isp.header.radar_conf.sar_swath.value,
            isp.header.radar_conf.tx_pulse_length,
            isp.header.radar_conf.tx_pulse_ramp_rate,
            isp.header.radar_conf.tx_pulse_start_freq,
            elev_beam_addr,
            azim_beam_addr,
            isp.header.radar_conf.ses_data.signal_type.value,
            isp.header.radar_conf.ses_data.tx_pulse_num,
            isp.header.radar_conf.ses_data.swap,
            isp.header.radar_conf.ses_data.swath,
        )
        rows.append(dat)

        prec_ebadr = isp.header.radar_conf.sas_data_imag.elev_beam_addr
        isp = InstrumentSourcePacket.fromFile(
            f=binary_strm, read_samples=False, offset=offset
        )
        offset += isp.packet_total_length

    df_hdr = DataFrame.from_records(data=rows[:-1], columns=columns)
    df_hdr.to_parquet(
        folder / "header.parquet",
    )
    tb = df_hdr["UTC"]
    ntb = len(tb)
    logger.info("Duration of acquisition: %.3f s" % (tb.max() - tb.min()))
    logger.info("Number of samples: %i" % df_hdr["number_samp"].sum())

    logger.info("Loading the orbit")
    if "orbit" in cfg.keys():
        orbit_path = cfg["orbit"]
    else:
        t_beg = datetime.utcfromtimestamp(tb[0])
        t_end = datetime.utcfromtimestamp(tb[ntb - 1])
        (f,) = listOrbitFiles(
            from_date=t_beg, to_date=t_end, platformnumber=platformnumber
        )
        orbit_path = downloadOrbit(file=f, dst_dir=folder)
    df_orbit = readOrbit(orbit_path)
    df_orbit.to_parquet(
        folder / "orbit.parquet",
    )

    # print(df_hdr['UTC'])
    # print(df_orbit['UTC'])

    if export_samples:
        logger.info("Loading the I/Q samples")
        isp = InstrumentSourcePacket.fromFile(
            f=binary_strm, read_samples=False, offset=offset0
        )
        dst_path = folder / "samples.bin"
        tot_ns = readBatchSamples(isp=isp, count=len(df_hdr.index), dst_path=dst_path)

        if tot_ns != df_hdr["number_samp"].sum():
            raise AssertionError(tot_ns, df_hdr["number_samp"].sum())


def loadDataTake(cfg: dict) -> Tuple[DataFrame, DataFrame, DSPSignal]:
    """Loads a data take processed by :func:`pysentinel1.process.processDataTake`

    Args:
      cfg
        The configuration, as loaded by :func:`pysentinel1.config.loadConfig`

    Returns:
      The orbit DataFrame
      The header DataFrame
      The DSPSignal built from the samples

    """
    folder = Path(cfg["output"])
    folder.mkdir(parents=True, exist_ok=True)

    orbit_path = folder / "orbit.parquet"
    df_orbit = pd.read_parquet(path=orbit_path)

    hdr_path = folder / "header.parquet"
    df_hdr = pd.read_parquet(path=hdr_path)

    samp_path = folder / "samples.bin"
    dt = np.dtype(np.complex128)
    ech = np.fromfile(samp_path, sep="", offset=0, dtype=dt)

    fs = df_hdr["samp_freq"][0]
    sig = DSPSignal(name="sig", samplingStart=0, samplingPeriod=1 / fs, y_serie=ech)

    return df_orbit, df_hdr, sig
