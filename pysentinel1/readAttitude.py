from typing import BinaryIO
import os
from struct import unpack

from tqdm import tqdm

from .ISPenum import WordIndex
from .InstrumentSourcePacket import InstrumentSourcePacket, PVTData, AttitudeData


__all__ = [
    "readFloatScalar",
    "readDoubleScalar",
    "syncReading",
    "readCCSDSCUCTime",
    "readPVT",
    "readAttitude",
    "exportPVTAttitude",
]


def readFloatScalar(isp: InstrumentSourcePacket) -> float:
    """Given the first Instrument Source Packet of a data word, reads the following ISPs to rebuild the float32 data

    Args:
      isp
        The first Instrument Source Packet that containes the sample we want

    Returns:
      The float32 value read

    """
    offset = isp.offset

    isp = InstrumentSourcePacket.fromFile(isp.stream, offset)
    if isp is None:
        return None, None
    value = isp.header.data_word

    isp = InstrumentSourcePacket.fromFile(isp.stream, offset + isp.packet_total_length)
    if isp is None:
        return None, None
    value = value + isp.header.data_word

    (res,) = unpack("f", value[::-1])
    return isp, res


def readDoubleScalar(isp: InstrumentSourcePacket) -> float:
    """Given the first Instrument Source Packet of a data word, reads the following ISPs to rebuild the float64 data

    Args:
      isp
        The first Instrument Source Packet that containes the sample we want

    Returns:
      The float64 value read

    """
    offset = isp.offset

    isp = InstrumentSourcePacket.fromFile(isp.stream, offset)
    if isp is None:
        return None, None

    value = isp.header.data_word

    for _ in range(3):
        isp = InstrumentSourcePacket.fromFile(isp.stream, offset)
        if isp is None:
            return None, None

        value = value + isp.header.data_word

        offset += isp.packet_total_length

    (res,) = unpack("d", value[::-1])
    return isp, res


def syncReading(isp: InstrumentSourcePacket, word: WordIndex) -> InstrumentSourcePacket:
    """Reads Instrument Source Packets until the DataWordIndex *word* is found

    Args:
      word
        The DataWordIndex to find

    Returns:
      The first Instrument Source Packets that contains the DataWordIndex *word*

    """
    offset = isp.offset

    while True:
        isp = InstrumentSourcePacket.fromFile(isp.stream, offset)
        if isp is None:
            return None

        offset += isp.packet_total_length

        if isp.header.data_word_index == word:
            break

    return isp


def readCCSDSCUCTime(isp: InstrumentSourcePacket) -> float:
    """Given the first Instrument Source Packet of a data word, reads the following ISPs to rebuild the CCSDS CUC Time

    Args:
      isp
        The first Instrument Source Packet that containes the sample we want

    Returns:
      The CCSDS CUC Time read

    """
    offset = isp.offset

    isp = InstrumentSourcePacket.fromFile(isp.stream, offset)
    if isp is None:
        return None, None, None

    value = isp.header.data_word[1]

    for _ in range(3):
        isp = InstrumentSourcePacket.fromFile(isp.stream, offset)
        if isp is None:
            return None, None, None

        (dlt,) = unpack(">H", isp.header.data_word)
        value = value * 2 ** 16 + dlt

        offset += isp.packet_total_length

    return isp, value, value / 2 ** 24


def readPVT(isp: InstrumentSourcePacket) -> PVTData:
    """Given a Instrument Source Packet, syncs up to the first POS_X_1 DataWordIndex, then reads position and velocity

    Args:
      isp
        The first Instrument Source Packet that containes the sample we want

    Returns:
      The PVT data read

    """
    pvt = PVTData()

    isp = syncReading(isp, WordIndex.POS_X_1)
    if isp is None:
        return None, None
    isp, pvt.px = readDoubleScalar(isp)
    if isp is None:
        return None, None

    isp = syncReading(isp, WordIndex.POS_Y_1)
    if isp is None:
        return None, None
    isp, pvt.py = readDoubleScalar(isp)
    if isp is None:
        return None, None

    isp = syncReading(isp, WordIndex.POS_Z_1)
    if isp is None:
        return None, None
    isp, pvt.pz = readDoubleScalar(isp)
    if isp is None:
        return None, None

    isp = syncReading(isp, WordIndex.VEL_X_1)
    if isp is None:
        return None, None
    isp, pvt.vx = readFloatScalar(isp)
    if isp is None:
        return None, None

    isp = syncReading(isp, WordIndex.VEL_Y_1)
    if isp is None:
        return None, None
    isp, pvt.vy = readFloatScalar(isp)
    if isp is None:
        return None, None

    isp = syncReading(isp, WordIndex.VEL_Z_1)
    if isp is None:
        return None, None
    isp, pvt.vz = readFloatScalar(isp)
    if isp is None:
        return None, None

    isp = syncReading(isp, WordIndex.PVT_TIME_1)
    if isp is None:
        return None, None
    isp, pvt.id, pvt.time = readCCSDSCUCTime(isp)
    if isp is None:
        return None, None

    return isp, pvt


def readAttitude(isp: InstrumentSourcePacket) -> AttitudeData:
    """Given a Instrument Source Packet, syncs up to the first ATT_Q0_1 DataWordIndex, then reads attitude and angular velocity

    Args:
      isp
        The first Instrument Source Packet that containes the sample we want

    Returns:
      The attitude data read

    """
    att = AttitudeData()

    isp = syncReading(isp, WordIndex.ATT_Q0_1)
    if isp is None:
        return None, None
    isp, att.q0 = readFloatScalar(isp)
    if isp is None:
        return None, None

    isp = syncReading(isp, WordIndex.ATT_Q1_1)
    if isp is None:
        return None, None
    isp, att.qx = readFloatScalar(isp)
    if isp is None:
        return None, None

    isp = syncReading(isp, WordIndex.ATT_Q2_1)
    if isp is None:
        return None, None
    isp, att.qy = readFloatScalar(isp)
    if isp is None:
        return None, None

    isp = syncReading(isp, WordIndex.ATT_Q3_1)
    if isp is None:
        return None, None
    isp, att.qz = readFloatScalar(isp)
    if isp is None:
        return None, None

    isp = syncReading(isp, WordIndex.OMEGA_X_1)
    if isp is None:
        return None, None
    isp, att.wx = readFloatScalar(isp)
    if isp is None:
        return None, None

    isp = syncReading(isp, WordIndex.OMEGA_Y_1)
    if isp is None:
        return None, None
    isp, att.wy = readFloatScalar(isp)
    if isp is None:
        return None, None

    isp = syncReading(isp, WordIndex.OMEGA_Z_1)
    if isp is None:
        return None, None
    isp, att.wz = readFloatScalar(isp)
    if isp is None:
        return None, None

    isp = syncReading(isp, WordIndex.ATT_TIME_1)
    if isp is None:
        return None, None
    isp, att.id, att.time = readCCSDSCUCTime(isp)
    if isp is None:
        return None, None

    return isp, att


def exportPVTAttitude(src: BinaryIO, dst: BinaryIO):
    """Given an input raw file and an output csv file, writes the PVT and attitude data in the CSV

    Args:
      src
        A stream to a raw file
      dst
        A stream to a CSV file

    """
    sze = os.fstat(src.fileno()).st_size
    isp = InstrumentSourcePacket.fromFile(src, 0)
    dst.write("t_pvt,px,py,pz,vx,vy,vz,t_att,q0,qx,qy,qz,wx,wt,wz\n")

    offset = 0
    psc = 1
    while psc != 0:
        isp = InstrumentSourcePacket.fromFile(f=src, offset=offset, read_samples=False)
        offset += isp.packet_total_length
        psc = isp.header.packet_sequence_count
    isp, pvt = readPVT(isp)
    isp, att = readAttitude(isp)

    id0 = pvt.id
    pbar = tqdm(total=sze)
    last_off = 0
    while True:
        dst.write(
            "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n"
            % (
                pvt.time,
                pvt.px,
                pvt.py,
                pvt.pz,
                pvt.vx,
                pvt.vy,
                pvt.vz,
                att.time,
                att.q0,
                att.qx,
                att.qy,
                att.qz,
                att.wx,
                att.wy,
                att.wz,
            )
        )

        pbar.update(isp.offset - last_off)
        last_off = isp.offset

        while pvt.id == id0:
            isp, pvt = readPVT(isp)
            if isp is None:
                return
            isp, att = readAttitude(isp)

        id0 = pvt.id
