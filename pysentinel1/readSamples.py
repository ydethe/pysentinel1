from struct import pack

from tqdm import tqdm
import numpy as np

from . import logger
from .InstrumentSourcePacket import InstrumentSourcePacket


__all__ = ["readBatchSamples"]


def readBatchSamples(isp: InstrumentSourcePacket, count: int, dst_path: str) -> int:
    """Reads a batch of Instrument Source Packets and concatenates the samples they contain

    Args:
      isp
        The first Instrument Source Packet that containes the sample we want
      count
        The number of Instrument Source Packet to be read
      dst_path
        The path to the file where the samples will be written

    Returns:
      The total number of samples read

    """
    dst = open(dst_path, "wb")

    tot_ns = 0
    offset = isp.offset
    for i in tqdm(range(count)):
        isp = InstrumentSourcePacket.fromFile(
            f=isp.stream, offset=offset, read_samples=True
        )
        offset = isp.offset + isp.packet_total_length
        ns = isp.header.radar_conf.number_quad * 2
        isp.samples.tofile(dst)
        tot_ns += ns

    dst.close()

    return tot_ns


def test():
    path = "C:\\Users\\blaudiy\\Downloads\\s1a-iw-raw-s-vv-20211026t232425-20211026t232457-040297-04c663.dat"
    src = open(
        path,
        "rb",
    )

    isp = InstrumentSourcePacket.fromFile(f=src, offset=22194996, read_samples=True)
    readBatchSamples(
        isp,
        count=805,
        dst_path="s1a-iw-raw-s-vv-20211026t232425-20211026t232457-040297-04c663-samples-swath11.bin",
    )
