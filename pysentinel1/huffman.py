from re import M
from typing import Tuple

import numpy as np

from .ISPenum import (
    BAQ_B0_LIST,
    BAQ_B1_LIST,
    BAQ_B2_LIST,
    BAQ_B3_LIST,
    BAQ_B4_LIST,
    NRL_DICT,
    SF_LIST,
)

tree_0 = ["0", "10", "110", "111"]
tree_1 = ["0", "10", "110", "1110", "1111"]
tree_2 = ["0", "10", "110", "1110", "11110", "111110", "111111"]
tree_3 = [
    "00",
    "01",
    "10",
    "110",
    "1110",
    "11110",
    "111110",
    "1111110",
    "11111110",
    "11111111",
]
tree_4 = [
    "00",
    "010",
    "011",
    "100",
    "101",
    "1100",
    "1101",
    "1110",
    "11110",
    "111110",
    "11111100",
    "11111101",
    "111111100",
    "111111101",
    "111111110",
    "111111111",
]


def read_bits(buffer: bytes, start: int, end: int) -> int:
    """

    Examples:
      >>> b = b"abcd" # 01100001 01100010 01100011 01100100
      >>> read_bits(buffer=b, start=1, end=10)
      389

    """
    byte_start_index = start // 8
    byte_end_index = (end - 1) // 8
    bit_end_index = end - 8 * byte_end_index

    nb_bits = end - start
    if byte_start_index == byte_end_index:
        byte = buffer[byte_start_index]
        tmp = byte >> (8 - bit_end_index)
        value = tmp & (2 ** nb_bits - 1)
    else:
        value = read_bits(buffer=buffer, start=start, end=(byte_start_index + 1) * 8)

        for k in range(byte_start_index + 1, byte_end_index):
            dlt = read_bits(buffer=buffer, start=k * 8, end=(k + 1) * 8)
            value = value << 8 | dlt

        dlt = read_bits(buffer=buffer, start=byte_end_index * 8, end=end)
        value = value << (end - byte_end_index * 8) | dlt

    return value


def huffman_decode(BRC: int, coded: bytes, start_bit: int = 0) -> Tuple[int, int]:
    """

    Examples:
      >>> b = b"\\x0f\\xbf" # 00001111 10111111
      >>> huffman_decode(BRC=2, coded=b, start_bit=4)
      (5, 10)
      >>> b = b"\\xff" # 11111111
      >>> huffman_decode(BRC=3, coded=b, start_bit=0)
      (9, 8)

    """
    if BRC == 0:
        tree = tree_0
    elif BRC == 1:
        tree = tree_1
    elif BRC == 2:
        tree = tree_2
    elif BRC == 3:
        tree = tree_3
    elif BRC == 4:
        tree = tree_4
    else:
        raise AssertionError(BRC)
    max_len = max([len(x) for x in tree])

    len_bit = 1
    while True:
        value = read_bits(buffer=coded, start=start_bit, end=start_bit + len_bit)
        sval = bin(value)[2:].zfill(len_bit)
        if sval in tree:
            dec_val = tree.index(sval)
            return dec_val, start_bit + len_bit
        else:
            len_bit += 1

        if len_bit > max_len:
            raise AssertionError(len_bit, max_len, sval)


def decodeHCode(BRC: int, coded: bytes, start_bit: int = 0) -> Tuple[int, int, int]:
    sgn = read_bits(buffer=coded, start=start_bit, end=start_bit + 1)
    val, pos = huffman_decode(BRC=BRC, coded=coded, start_bit=start_bit + 1)
    return sgn, val, pos


def decodeSvalue(BRC: int, THIDX: int, sgn: int, Mcode: int) -> float:
    """
    Examples:
      >>> b = b"\\x0f\\xbf" # 00001111 10111111
      >>> THIDX = 239
      >>> sgn,Mcode,pos=decodeHCode(BRC=2, coded=b, start_bit=3)
      >>> x=decodeSvalue(BRC=2, THIDX=THIDX,sgn=sgn,Mcode=Mcode)
      >>> x
      601.727311
      >>> b = b"\\xff\\xff" # 111111111 111111111
      >>> THIDX = 3
      >>> sgn,Mcode,pos=decodeHCode(BRC=3, coded=b)
      >>> x=decodeSvalue(BRC=3, THIDX=THIDX,sgn=sgn,Mcode=Mcode)
      >>> x
      -9.0
      >>> b = b"\\xff\\xff" # 111111111 111111111
      >>> THIDX = 5
      >>> sgn,Mcode,pos=decodeHCode(BRC=3, coded=b)
      >>> x=decodeSvalue(BRC=3, THIDX=THIDX,sgn=sgn,Mcode=Mcode)
      >>> x
      -9.5

    """
    if BRC == 0 and THIDX <= 3 and Mcode < 3:
        res = (-1) ** sgn * Mcode
    elif BRC == 0 and THIDX <= 3 and Mcode == 3:
        B0 = BAQ_B0_LIST[THIDX]
        res = (-1) ** sgn * B0
    elif BRC == 0 and THIDX > 3:
        NRL = NRL_DICT[(BRC, Mcode)]
        SF = SF_LIST[THIDX]
        res = (-1) ** sgn * NRL * SF

    elif BRC == 1 and THIDX <= 3 and Mcode < 4:
        res = (-1) ** sgn * Mcode
    elif BRC == 1 and THIDX <= 3 and Mcode == 4:
        B1 = BAQ_B1_LIST[THIDX]
        res = (-1) ** sgn * B1
    elif BRC == 1 and THIDX > 3:
        NRL = NRL_DICT[(BRC, Mcode)]
        SF = SF_LIST[THIDX]
        res = (-1) ** sgn * NRL * SF

    elif BRC == 2 and THIDX <= 5 and Mcode < 6:
        res = (-1) ** sgn * Mcode
    elif BRC == 2 and THIDX <= 5 and Mcode == 6:
        B2 = BAQ_B2_LIST[THIDX]
        res = (-1) ** sgn * B2
    elif BRC == 2 and THIDX > 5:
        NRL = NRL_DICT[(BRC, Mcode)]
        SF = SF_LIST[THIDX]
        res = (-1) ** sgn * NRL * SF

    elif BRC == 3 and THIDX <= 6 and Mcode < 9:
        res = (-1) ** sgn * Mcode
    elif BRC == 3 and THIDX <= 6 and Mcode == 9:
        B3 = BAQ_B3_LIST[THIDX]
        res = (-1) ** sgn * B3
    elif BRC == 3 and THIDX > 6:
        NRL = NRL_DICT[(BRC, Mcode)]
        SF = SF_LIST[THIDX]
        res = (-1) ** sgn * NRL * SF

    elif BRC == 4 and THIDX <= 8 and Mcode < 15:
        res = (-1) ** sgn * Mcode
    elif BRC == 4 and THIDX <= 8 and Mcode == 15:
        B4 = BAQ_B4_LIST[THIDX]
        res = (-1) ** sgn * B4
    elif BRC == 4 and THIDX > 8:
        NRL = NRL_DICT[(BRC, Mcode)]
        SF = SF_LIST[THIDX]
        res = (-1) ** sgn * NRL * SF

    return res


if __name__ == "__main__":
    import doctest

    doctest.testmod()
