from pkg_resources import get_distribution
import logging

from blocksim.LogFormatter import LogFormatter

__version__ = get_distribution(__name__).version
__author__ = "Y. BLAUDIN DE THE"
__email__ = "ydethe@gmail.com"

logger = logging.getLogger("pysentinel1_logger")
logger.setLevel(logging.DEBUG)

# création d'un formateur qui va ajouter le temps, le niveau
# de chaque message quand on écrira un message dans le log
formatter = LogFormatter()
# création d'un handler qui va rediriger chaque écriture de log
# sur la console
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)
