from pathlib import Path
import requests
from requests.auth import HTTPBasicAuth
from datetime import datetime, timedelta
from dataclasses import dataclass
from typing import List

from . import logger


__all__ = ["listOrbitFiles", "downloadOrbit"]


@dataclass(init=False, repr=True)
class File:
    begin: datetime
    end: datetime
    link: str


def listOrbitFiles(
    from_date: datetime, to_date: datetime, platformnumber: str
) -> List[File]:
    """Lists all orbit files available for download from scihub.copernicus.eu
    The filters are:

    * AUX_POEORB for precise orbit
    * Sentinel-1
    The function finds orbits that cover the IQ samples to be processed

    Args:
      from_date
        The beginning of IQ samples to process
      to_date
        The end of IQ samples to process
      platformnumber
        "A" for Sentinel-1A, "B" for Sentinel-1B

    Returns:
      The list of the orbit file found

    """
    dt_fmt = "%Y-%m-%dT%H:%M:%SZ"
    delta = timedelta(days=2)
    fds = from_date.strftime(dt_fmt)
    tds = to_date.strftime(dt_fmt)
    fds_delta = (from_date - delta).strftime(dt_fmt)
    tds_delta = (to_date + delta).strftime(dt_fmt)
    params = {
        "q": "beginPosition:[%s TO %s] AND endPosition:[%s TO %s] AND producttype:AUX_POEORB AND platformname:Sentinel-1 AND filename:S1%s_*"
        % (fds_delta, fds, tds, tds_delta, platformnumber),
        "format": "json",
    }
    logger.info("Orbit query URL: https://scihub.copernicus.eu/gnss/search")
    logger.info("Orbit query params: %s", params)
    r = requests.get(
        url="https://scihub.copernicus.eu/gnss/search",
        params=params,
        auth=HTTPBasicAuth("gnssguest", "gnssguest"),
    )
    dat = r.json()
    if not "entry" in dat["feed"].keys():
        return []

    if isinstance(dat["feed"]["entry"], dict):
        entries = [dat["feed"]["entry"]]
    else:
        entries = dat["feed"]["entry"]

    nf = len(entries)
    res = []

    for k in range(nf):
        file = File()

        for d in entries[k]["date"]:
            if d["name"] == "beginposition":
                # 2021-10-01T22:59:42Z
                file.begin = datetime.strptime(d["content"], dt_fmt)
                # print(d["content"])
            elif d["name"] == "endposition":
                file.end = datetime.strptime(d["content"], dt_fmt)
                # print(d["content"])
            else:
                pass

        for d in entries[k]["link"]:
            if not "rel" in d.keys():
                file.link = d["href"]

        res.append(file)

    return res


def downloadOrbit(file: File, dst_dir: str) -> Path:
    """Downloads an orbit file

    Args:
      file
        The file to download, determined by a previous call to :func:`pysentinel1.downloadOrbitFile.listOrbitFiles`
      dst_dir
        The folder where the file is downloaded

    Returns:
      The path of the downloaded orbit file

    """
    url = file.link
    folder = Path(dst_dir)
    folder.mkdir(parents=True, exist_ok=True)
    path = folder / "s1a_raw_aux_poeorb.xml"

    logger.info("Downloaded orbit URL: %s" % url)

    with requests.get(
        url, stream=True, auth=HTTPBasicAuth("gnssguest", "gnssguest")
    ) as r:
        with open(path, "wb") as f:
            for chunk in r.iter_content(chunk_size=16 * 1024):
                f.write(chunk)

    return path


if __name__ == "__main__":
    beg = datetime(year=2021, month=10, day=20, hour=3, minute=30)
    end = datetime(year=2021, month=10, day=20, hour=3, minute=31, second=30)
    (f,) = listOrbitFiles(beg, end, platformnumber="A")
    downloadOrbit(file=f, dst_dir="tmp")
