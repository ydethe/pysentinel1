import numpy as np

from .ISPenum import BAQMode
from .huffman import read_bits, decodeHCode, decodeSvalue


__all__ = ["decodeFormatDSamples"]


def decodeFormatDSamples(NQ: int, baq_mode: BAQMode, raw: bytes) -> np.array:
    """Decodes raw samples into a complex numpy array.
    Not intended to be used directly

    Args:
      NQ
        Number of quads expected
      baq_mode
        Format of the raw samples
      raw
        Raw samples

    Returns:
      The complex samples

    """
    NB = int(np.ceil(2 * NQ / 256))

    BRC = np.empty(NB, dtype=np.uint8)
    THIDX = np.empty(NB, dtype=np.uint8)
    IE_Mcodes = np.empty(NQ, dtype=np.uint8)
    IO_Mcodes = np.empty(NQ, dtype=np.uint8)
    QE_Mcodes = np.empty(NQ, dtype=np.uint8)
    QO_Mcodes = np.empty(NQ, dtype=np.uint8)

    IE_signs = np.empty(NQ, dtype=np.uint8)
    IO_signs = np.empty(NQ, dtype=np.uint8)
    QE_signs = np.empty(NQ, dtype=np.uint8)
    QO_signs = np.empty(NQ, dtype=np.uint8)

    # ==========================================================
    # MCode, sign, BRC and THIDX reading
    # ==========================================================

    pos = 0  # Reading position in *bits*

    # Reading IE Huffman Codes
    iHcode = 0
    for b in range(NB):
        BRC[b] = read_bits(buffer=raw, start=pos, end=pos + 3)
        pos += 3
        # print("b=%i/%i, BRC=%i" % (b,NB,BRC[b]))

        if b < NB - 1:
            ncode = 128
        else:
            ncode = NQ - 128 * (NB - 1)
        for _ in range(ncode):
            sgn, val, pos = decodeHCode(BRC[b], raw, start_bit=pos)
            IE_signs[iHcode] = sgn
            IE_Mcodes[iHcode] = val
            iHcode += 1

    rm = pos % 16
    if rm != 0:
        pos += 16 - rm

    # Reading IO Huffman Codes
    iHcode = 0
    for b in range(NB):
        if b < NB - 1:
            ncode = 128
        else:
            ncode = NQ - 128 * (NB - 1)
        for _ in range(ncode):
            sgn, val, pos = decodeHCode(BRC[b], raw, start_bit=pos)
            IO_signs[iHcode] = sgn
            IO_Mcodes[iHcode] = val
            iHcode += 1

    rm = pos % 16
    if rm != 0:
        pos += 16 - rm

    # Reading QE Huffman Codes
    iHcode = 0
    for b in range(NB):
        THIDX[b] = read_bits(buffer=raw, start=pos, end=pos + 8)
        pos += 8
        # print("b=%i/%i, THIDX=%i" % (b,NB,THIDX[b]))

        if b < NB - 1:
            ncode = 128
        else:
            ncode = NQ - 128 * (NB - 1)
        for _ in range(ncode):
            sgn, val, pos = decodeHCode(BRC[b], raw, start_bit=pos)
            QE_signs[iHcode] = sgn
            QE_Mcodes[iHcode] = val
            iHcode += 1

    rm = pos % 16
    if rm != 0:
        pos += 16 - rm

    # Reading QO Huffman Codes
    iHcode = 0
    for b in range(NB):
        if b < NB - 1:
            ncode = 128
        else:
            ncode = NQ - 128 * (NB - 1)
        for _ in range(ncode):
            sgn, val, pos = decodeHCode(BRC[b], raw, start_bit=pos)
            QO_signs[iHcode] = sgn
            QO_Mcodes[iHcode] = val
            iHcode += 1

    rm = pos % 16
    if rm != 0:
        pos += 16 - rm

    # ==========================================================
    # Svalue computation
    # ==========================================================

    IE_Svalue = np.empty(NQ, dtype=np.float64)
    IO_Svalue = np.empty(NQ, dtype=np.float64)
    QE_Svalue = np.empty(NQ, dtype=np.float64)
    QO_Svalue = np.empty(NQ, dtype=np.float64)

    # Reading IE Svalue
    iHcode = 0
    for b in range(NB):
        if b < NB - 1:
            ncode = 128
        else:
            ncode = NQ - 128 * (NB - 1)
        for _ in range(ncode):
            sgn = IE_signs[iHcode]
            Mcode = IE_Mcodes[iHcode]
            IE_Svalue[iHcode] = decodeSvalue(
                BRC=BRC[b], THIDX=THIDX[b], sgn=sgn, Mcode=Mcode
            )
            iHcode += 1

    # Reading IO Svalue
    iHcode = 0
    for b in range(NB):
        if b < NB - 1:
            ncode = 128
        else:
            ncode = NQ - 128 * (NB - 1)
        for _ in range(ncode):
            sgn = IO_signs[iHcode]
            Mcode = IO_Mcodes[iHcode]
            IO_Svalue[iHcode] = decodeSvalue(
                BRC=BRC[b], THIDX=THIDX[b], sgn=sgn, Mcode=Mcode
            )
            iHcode += 1

    # Reading QE Svalue
    iHcode = 0
    for b in range(NB):
        if b < NB - 1:
            ncode = 128
        else:
            ncode = NQ - 128 * (NB - 1)
        for _ in range(ncode):
            sgn = QE_signs[iHcode]
            Mcode = QE_Mcodes[iHcode]
            QE_Svalue[iHcode] = decodeSvalue(
                BRC=BRC[b], THIDX=THIDX[b], sgn=sgn, Mcode=Mcode
            )
            iHcode += 1

    # Reading QO Svalue
    iHcode = 0
    for b in range(NB):
        if b < NB - 1:
            ncode = 128
        else:
            ncode = NQ - 128 * (NB - 1)
        for _ in range(ncode):
            sgn = QO_signs[iHcode]
            Mcode = QO_Mcodes[iHcode]
            QO_Svalue[iHcode] = decodeSvalue(
                BRC=BRC[b], THIDX=THIDX[b], sgn=sgn, Mcode=Mcode
            )
            iHcode += 1

    samples = np.empty(2 * NQ, dtype=np.complex128)
    for j in range(NQ):
        samples[2 * j] = IE_Svalue[j] + 1j * QE_Svalue[j]
        samples[2 * j + 1] = IO_Svalue[j] + 1j * QO_Svalue[j]

    return samples
