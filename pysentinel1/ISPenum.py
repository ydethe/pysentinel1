from enum import Enum
from fractions import Fraction


# https://sentinel.esa.int/documents/247904/2142675/Sentinel-1-SAR-Space-Packet-Protocol-Data-Unit.pdf


class ECCNumber(Enum):
    STRIPMAP_1 = 1
    STRIPMAP_2 = 2
    STRIPMAP_3 = 3
    STRIPMAP_4 = 4
    STRIPMAP_5N = 5  # Used for Stripmap 5 imaging on northern hemisphere
    STRIPMAP_6 = 6
    IW_SWATH = 8  # Interferometric Wide Swath
    WAVE_MODE = (
        9  # Leapfrog mode using alternating vignettes at different incidence angles
    )
    STRIPMAP_5S = 10  # Used for Stripmap 5 imaging on southern hemisphere
    STRIPMAP_1_WO_CAL = 11
    STRIPMAP_2_WO_CAL = 12
    STRIPMAP_3_WO_CAL = 13
    STRIPMAP_4_WO_CAL = 14
    RFC_MODE = 15  # RFcharacterisation mode based on PCC sequences (RF672 for EFEs and PCC 32 for TAs )
    TEST_MODE = 16
    ELEVATION_NOTCH_S3 = 17  # Elevation Notch in centre of S3 swath
    AZIMUTH_NOTCH_S1 = 18
    AZIMUTH_NOTCH_S2 = 19
    AZIMUTH_NOTCH_S3 = 20
    AZIMUTH_NOTCH_S4 = 21
    AZIMUTH_NOTCH_S5N = (
        22  # Used for Az. Notch Mode in Stripmap 5 on northern hemisphere
    )
    AZIMUTH_NOTCH_S5S = (
        23  # Used for Az. Notch Mode in Stripmap 5 on southern hemisphere
    )
    AZIMUTH_NOTCH_S6 = 24
    STRIPMAP_5N_WO_CAL = 25
    STRIPMAP_5S_WO_CAL = 26
    STRIPMAP_6_WO_CAL = 27
    ELEVATION_NOTCH_S3_WO_CAL = 31
    EXTRA_WIDE_SWATH = 32
    AZIMUTH_NOTCH_S1_WO_CAL = 33
    AZIMUTH_NOTCH_S3_WO_CAL = 34
    AZIMUTH_NOTCH_S6_WO_CAL = 35
    NOISE_CHARACTERISATION_S1 = 37
    NOISE_CHARACTERISATION_S2 = 38
    NOISE_CHARACTERISATION_S3 = 39
    NOISE_CHARACTERISATION_S4 = 40
    NOISE_CHARACTERISATION_S5N = 41
    NOISE_CHARACTERISATION_S5S = 42
    NOISE_CHARACTERISATION_S6 = 43
    NOISE_CHARACTERISATION_EWS = 44
    NOISE_CHARACTERISATION_IWS = 45
    NOISE_CHARACTERISATION_WAVE = 46


class WordIndex(Enum):
    NONE = 0
    POS_X_1 = 1
    POS_X_2 = 2
    POS_X_3 = 3
    POS_X_4 = 4
    POS_Y_1 = 5
    POS_Y_2 = 6
    POS_Y_3 = 7
    POS_Y_4 = 8
    POS_Z_1 = 9
    POS_Z_2 = 10
    POS_Z_3 = 11
    POS_Z_4 = 12
    VEL_X_1 = 13
    VEL_X_2 = 14
    VEL_Y_1 = 15
    VEL_Y_2 = 16
    VEL_Z_1 = 17
    VEL_Z_2 = 18
    PVT_TIME_1 = 19
    PVT_TIME_2 = 20
    PVT_TIME_3 = 21
    PVT_TIME_4 = 22
    ATT_Q0_1 = 23
    ATT_Q0_2 = 24
    ATT_Q1_1 = 25
    ATT_Q1_2 = 26
    ATT_Q2_1 = 27
    ATT_Q2_2 = 28
    ATT_Q3_1 = 29
    ATT_Q3_2 = 30
    OMEGA_X_1 = 31
    OMEGA_X_2 = 32
    OMEGA_Y_1 = 33
    OMEGA_Y_2 = 34
    OMEGA_Z_1 = 35
    OMEGA_Z_2 = 36
    ATT_TIME_1 = 37
    ATT_TIME_2 = 38
    ATT_TIME_3 = 39
    ATT_TIME_4 = 40
    POINTING_STATUS = 41


class RXChannel(Enum):
    RX_POL_V = 0
    RX_POL_H = 1


class RadarErrorFlag(Enum):
    NOMINAL = 0
    SES_SSB_PARITY_ERROR = 1


class BAQMode(Enum):
    BYPASS = 0
    BAQ_3_BIT = 3  # No Entropy Coding
    BAQ_4_BIT = 4  # No Entropy Coding
    BAQ_5_BIT = 5  # No Entropy Coding
    FDBAQ_0 = 12  # Mode 0 nominal FDBAQ
    FDBAQ_1 = 13  # FDBAQ with first alternative rate selection thresholds
    FDBAQ_2 = 14  # FDBAQ with second alternative rate selection thresholds


class SARSwath(Enum):
    FULL_BW = 0
    S1_WV1 = 1
    S2 = 3
    S3 = 4
    S4 = 5
    S5 = 6
    EW1 = 7
    IW1 = 8
    S6_IW3 = 9
    EW_2_3_4_5 = 10
    IW2_WV2 = 11


class SASMode(Enum):
    IMAGING_NOISE = 0
    CALIBRATION = 1


class Polarisation(Enum):
    TX_H = 0
    TX_H_RX_H = 1
    TX_H_RX_V = 2
    TX_H_RX_C = 3
    TX_V = 4
    TX_V_RX_H = 5
    TX_V_RX_V = 6
    TX_V_RX_C = 7


class SASTestMode(Enum):
    SAS_TEST_MODE_ACTIVE = 0
    NORMAL_CALIB_MODE = 1


class CalibType(Enum):
    TX_CAL = 0
    RX_CAL = 1
    EPDN_CAL = 2
    TA_CAL = 3
    APDN_CAL = 4
    TXH_CAL_ISO = 7  # Tx Cal Isolation at Tx-Polarisation H


class SignalType(Enum):
    ECHO = 0  # Radar echo signal (nominal SAR imaging)
    NOISE = 1  # Noise measurement
    TX_CAL = 8
    RX_CAL = 9
    EPDN_CAL = 10
    TA_CAL = 11
    APDN_CAL = 12
    TXH_CAL_ISO = 15


FILTER_BW_DICT = {
    0: 100.0,
    1: 87.71,
    3: 74.25,
    4: 59.44,
    5: 50.62,
    6: 44.89,
    7: 22.2,
    8: 56.59,
    9: 42.86,
    10: 15.1,
    11: 48.35,
}

DECIM_RATIO_DICT = {
    0: Fraction(3, 4),
    1: Fraction(2, 3),
    3: Fraction(5, 9),
    4: Fraction(4, 9),
    5: Fraction(3, 8),
    6: Fraction(1, 3),
    7: Fraction(1, 6),
    8: Fraction(3, 7),
    9: Fraction(5, 16),
    10: Fraction(3, 26),
    11: Fraction(4, 11),
}

FILTER_LENGTH_DICT = {
    0: 28,
    1: 28,
    3: 32,
    4: 40,
    5: 48,
    6: 52,
    7: 92,
    8: 36,
    9: 68,
    10: 120,
    11: 44,
}

FILTER_OUTPUT_OFFSETS = {
    0: 87,
    1: 87,
    3: 88,
    4: 90,
    5: 92,
    6: 93,
    7: 103,
    8: 89,
    9: 97,
    10: 110,
    11: 91,
}

# (Filter No., C)
D_VALUES = {
    (0, 0): 1,
    (0, 1): 1,
    (0, 2): 2,
    (0, 3): 3,
    (1, 0): 1,
    (1, 1): 1,
    (1, 2): 2,
    (3, 0): 1,
    (3, 1): 1,
    (3, 2): 2,
    (3, 3): 2,
    (3, 4): 3,
    (3, 5): 3,
    (3, 6): 4,
    (3, 7): 4,
    (3, 8): 5,
    (4, 0): 0,
    (4, 1): 1,
    (4, 2): 1,
    (4, 3): 2,
    (4, 4): 2,
    (4, 5): 3,
    (4, 6): 3,
    (4, 7): 4,
    (4, 8): 4,
    (5, 0): 0,
    (5, 1): 1,
    (5, 2): 1,
    (5, 3): 1,
    (5, 4): 2,
    (5, 5): 2,
    (5, 6): 3,
    (5, 7): 3,
    (6, 0): 0,
    (6, 1): 0,
    (6, 2): 1,
    (7, 0): 0,
    (7, 1): 0,
    (7, 2): 0,
    (7, 3): 0,
    (7, 4): 0,
    (7, 5): 1,
    (8, 0): 0,
    (8, 1): 1,
    (8, 2): 1,
    (8, 3): 2,
    (8, 4): 2,
    (8, 5): 3,
    (8, 6): 3,
    (9, 0): 0,
    (9, 1): 0,
    (9, 2): 1,
    (9, 3): 1,
    (9, 4): 1,
    (9, 5): 2,
    (9, 6): 2,
    (9, 7): 2,
    (9, 8): 2,
    (9, 9): 3,
    (9, 10): 3,
    (9, 11): 3,
    (9, 12): 4,
    (9, 13): 4,
    (9, 14): 4,
    (9, 15): 5,
    (10, 0): 0,
    (10, 1): 0,
    (10, 2): 0,
    (10, 3): 0,
    (10, 4): 0,
    (10, 5): 0,
    (10, 6): 0,
    (10, 7): 1,
    (10, 8): 1,
    (10, 9): 1,
    (10, 10): 1,
    (10, 11): 1,
    (10, 12): 1,
    (10, 13): 1,
    (10, 14): 1,
    (10, 15): 1,
    (10, 16): 2,
    (10, 17): 2,
    (10, 18): 2,
    (10, 19): 2,
    (10, 20): 2,
    (10, 21): 2,
    (10, 22): 2,
    (10, 23): 2,
    (10, 24): 3,
    (10, 25): 3,
    (11, 0): 0,
    (11, 1): 1,
    (11, 2): 1,
    (11, 3): 1,
    (11, 4): 2,
    (11, 5): 2,
    (11, 6): 3,
    (11, 7): 3,
    (11, 8): 3,
    (11, 9): 4,
    (11, 10): 4,
}

BAQ_B0_LIST = [3.0, 3.0, 3.16, 3.53]
BAQ_B1_LIST = [4.0, 4.0, 4.08, 4.37]
BAQ_B2_LIST = [6.0, 6.0, 6.0, 6.15, 6.5, 6.88]
BAQ_B3_LIST = [9.0, 9.0, 9.0, 9.0, 9.36, 9.5, 10.1]
BAQ_B4_LIST = [15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.22, 15.5, 16.05]

# Normalised Reconstruction Levels table
# Index : BRC, then Mcode
NRL_DICT = {
    (0, 0): 0.3637,
    (0, 1): 1.0915,
    (0, 2): 1.8208,
    (0, 3): 2.6406,
    (1, 0): 0.3042,
    (1, 1): 0.9127,
    (1, 2): 1.5216,
    (1, 3): 2.1313,
    (1, 4): 2.8426,
    (2, 0): 0.2305,
    (2, 1): 0.6916,
    (2, 2): 1.1528,
    (2, 3): 2.6140,
    (2, 4): 2.0754,
    (2, 5): 2.5369,
    (2, 6): 3.1191,
    (3, 0): 0.1702,
    (3, 1): 0.5107,
    (3, 2): 0.8511,
    (3, 3): 1.1916,
    (3, 4): 1.5321,
    (3, 5): 1.8726,
    (3, 6): 2.2131,
    (3, 7): 2.5536,
    (3, 8): 2.8942,
    (3, 9): 3.3744,
    (4, 0): 0.1130,
    (4, 1): 0.3389,
    (4, 2): 0.5649,
    (4, 3): 0.7908,
    (4, 4): 1.0167,
    (4, 5): 1.2428,
    (4, 6): 1.4687,
    (4, 7): 1.6947,
    (4, 8): 1.9206,
    (4, 9): 2.1466,
    (4, 10): 2.3725,
    (4, 11): 2.5985,
    (4, 12): 2.8244,
    (4, 13): 3.0504,
    (4, 14): 3.2764,
    (4, 15): 3.6623,
}

# Sima Factors table
SF_LIST = [
    0,
    0.63,
    1.25,
    1.88,
    2.51,
    3.13,
    3.76,
    4.39,
    5.01,
    5.64,
    6.27,
    6.89,
    7.52,
    8.15,
    8.77,
    9.4,
    10.03,
    10.65,
    11.28,
    11.91,
    12.53,
    13.16,
    13.79,
    14.41,
    15.04,
    15.67,
    16.29,
    16.92,
    17.55,
    18.17,
    18.8,
    19.43,
    20.05,
    20.68,
    21.31,
    21.93,
    22.56,
    23.19,
    23.81,
    24.44,
    25.07,
    25.69,
    26.32,
    26.95,
    27.57,
    28.2,
    28.83,
    29.45,
    30.08,
    30.71,
    31.33,
    31.96,
    32.59,
    33.21,
    33.84,
    34.47,
    35.09,
    35.72,
    36.35,
    36.97,
    37.6,
    38.23,
    38.85,
    39.48,
    40.11,
    40.73,
    41.36,
    41.99,
    42.61,
    43.24,
    43.87,
    44.49,
    45.12,
    45.75,
    46.37,
    47,
    47.63,
    48.25,
    48.88,
    49.51,
    50.13,
    50.76,
    51.39,
    52.01,
    52.64,
    53.27,
    53.89,
    54.52,
    55.15,
    55.77,
    56.4,
    57.03,
    57.65,
    58.28,
    58.91,
    59.53,
    60.16,
    60.79,
    61.41,
    62.04,
    62.98,
    64.24,
    65.49,
    66.74,
    68,
    69.25,
    70.5,
    71.76,
    73.01,
    74.26,
    75.52,
    76.77,
    78.02,
    79.28,
    80.53,
    81.78,
    83.04,
    84.29,
    85.54,
    86.8,
    88.05,
    89.3,
    90.56,
    91.81,
    93.06,
    94.32,
    95.57,
    96.82,
    98.08,
    99.33,
    100.58,
    101.84,
    103.09,
    104.34,
    105.6,
    106.85,
    108.1,
    109.35,
    110.61,
    111.86,
    113.11,
    114.37,
    115.62,
    116.87,
    118.13,
    119.38,
    120.63,
    121.89,
    123.14,
    124.39,
    125.65,
    126.9,
    128.15,
    129.41,
    130.66,
    131.91,
    133.17,
    134.42,
    135.67,
    136.93,
    138.18,
    139.43,
    140.69,
    141.94,
    143.19,
    144.45,
    145.7,
    146.95,
    148.21,
    149.46,
    150.71,
    151.97,
    153.22,
    154.47,
    155.73,
    156.98,
    158.23,
    159.49,
    160.74,
    161.99,
    163.25,
    164.5,
    165.75,
    167.01,
    168.26,
    169.51,
    170.77,
    172.02,
    173.27,
    174.53,
    175.78,
    177.03,
    178.29,
    179.54,
    180.79,
    182.05,
    183.3,
    184.55,
    185.81,
    187.06,
    188.31,
    189.57,
    190.82,
    192.07,
    193.33,
    194.58,
    195.83,
    197.09,
    198.34,
    199.59,
    200.85,
    202.1,
    203.35,
    204.61,
    205.86,
    207.11,
    208.37,
    209.62,
    210.87,
    212.13,
    213.38,
    214.63,
    215.89,
    217.14,
    218.39,
    219.65,
    220.9,
    222.15,
    223.41,
    224.66,
    225.91,
    227.17,
    228.42,
    229.67,
    230.93,
    232.18,
    233.43,
    234.69,
    235.94,
    237.19,
    238.45,
    239.7,
    240.95,
    242.21,
    243.46,
    244.71,
    245.97,
    247.22,
    248.47,
    249.73,
    250.98,
    252.23,
    253.49,
    254.74,
    255.99,
    255.99,
]
