from datetime import datetime

import pandas as pd
from pandas import DataFrame


__all__ = ["readOrbit"]


def readOrbit(path: str) -> DataFrame:
    """Reads a downloaded orbit file (see :func:`pysentinel1.downloadOrbitFile.downloadOrbit`).
    Turns the file into a pandas DataFrame

    Arg:
      path
        The path to the downloaded orbit file

    Returns:
      The DataFrame

    """
    df = pd.read_xml(
        path,
        xpath="./Data_Block/List_of_OSVs/OSV",
    )
    dt = pd.to_datetime(df["UTC"], format="UTC=%Y-%m-%dT%H:%M:%S.%f")
    df["UTC"] = dt.view("int64") / 10 ** 9
    del df["TAI"]
    del df["UT1"]

    return df
