// Huffman.h
// https://www.codeproject.com/Articles/9021/Simple-and-fast-Huffman-coding
//////////////////////////////////////////////////////////////////////

#if !defined(__COMPRESS_H__)
#define __COMPRESS_H__

#define BYTE char

bool CompressHuffman(BYTE *pSrc, int nSrcLen, BYTE *&pDes, int &nDesLen);
bool DecompressHuffman(BYTE *pSrc, int nSrcLen, BYTE *&pDes, int &nDesLen);

#endif __COMPRESS_H__
