.. Licensed under the MIT Licensed

.. _install:

============
Installation
============

Install with pip
----------------

.. highlight:: console

The project is hosted on pypi : `<https://pypi.org/project/pysentinel1/>`_.

You can install pysentinel1 in the usual ways. The simplest way is with pip::

    $ pip install pysentinel1

Checking the installation
-------------------------

If all went well, you should be able to open a command prompt, and see
pysentinel1 installed properly:

.. parsed-literal::

    $ python -m pysentinel1 --version
    pysentinel1, version |release|
    Documentation at https://pysentinel1.readthedocs.io/en/latest/
