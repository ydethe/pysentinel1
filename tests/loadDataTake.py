from blocksim.dsp.DSPSignal import DSPSignal
from blocksim.dsp.DSPSignal import DSPSignal

from pysentinel1.config import loadConfig
from pysentinel1.process import loadDataTake


print("Reading file")
cfg = loadConfig("tests/config.yml")
df_orbit, df_hdr, sig = loadDataTake(cfg)

print("Creating replica")
dt_ech = sig.samplingPeriod
tau = df_hdr["tx_pulse_length"][0]
fstart = df_hdr["tx_pulse_start_freq"][0]
fend = df_hdr["tx_pulse_ramp_rate"][0] * tau + fstart
rep = DSPSignal.fromLinearFM(
    name="rep",
    samplingStart=0,
    samplingPeriod=dt_ech,
    tau=tau,
    fstart=fstart,
    fend=fend,
)

print("Correlation")
corr = sig.correlate(rep, win="hamming")
