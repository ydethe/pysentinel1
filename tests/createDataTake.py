from pysentinel1.config import loadConfig
from pysentinel1.process import processDataTake


cfg = loadConfig("tests/config.yml")
processDataTake(cfg, export_samples=True)
